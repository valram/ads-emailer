from django.db import models


class Subscriber(models.Model):
    email = models.EmailField(unique=True)
    joined = models.DateTimeField(auto_now_add=True)
    activated = models.BooleanField(default=False)
    telegram_id = models.PositiveIntegerField(null=True, unique=True)

    def __str__(self):
        return self.email
