from django.contrib import admin


from .models import Subscriber


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):

    list_display = [
        'email',
        'joined',
        'activated',
        'autoru_subscriptions',
    ]
