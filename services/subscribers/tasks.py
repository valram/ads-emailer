from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings


import requests
from celery.utils.log import get_task_logger
from ads_emailer import settings
from ads_emailer.celery import app
from auto_ru.models import AutoruRubric
from auto_ru.models import AutoruAdv


logger = get_task_logger(__name__)
TOKEN = settings.TELEGRAM_BOT_TOKEN


@app.task
def email_new_ads_to_subscribers(count_ads=10, **kwargs):
    subject = 'New auto.ru ads'

    for rubric in AutoruRubric.objects.all():
        ads = AutoruAdv.objects.filter(
            emailed=False).filter(rubric=rubric,).order_by('created')
        if ads:
            html = render_to_string(
                'subscribers/new_ads_email.html',
                {'ads': ads[:count_ads],}
            )
            message = EmailMessage(
                subject,
                html,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[subscriber.email for subscriber in rubric.subscribers.all()]
            )
            message.content_subtype = 'html'
            message.send()

            ads.update(emailed=True)


@app.task
def telegram_new_ads_to_subscribers(count_ads=10, **kwargs):

    for rubric in AutoruRubric.objects.all():
        ads = AutoruAdv.objects.filter(
            telegramed=False).filter(rubric=rubric,).order_by('created')

        if ads:
            for subscriber in rubric.subscribers.all().distinct():
                requests.post(
                    'https://api.telegram.org/bot{TOKEN}/sendMessage'.format(TOKEN=TOKEN),
                    data={
                        'chat_id': subscriber.telegram_id,
                        'text': render_to_string('subscribers/new_ads_telegram.html',
                                                {'ads': ads[:count_ads], }),
                        'parse_mode' : 'html',
                    }
                )
            ads.update(telegramed=True)
