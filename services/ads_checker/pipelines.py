from auto_ru.models import AutoruAdv


class AutoruAdvPipeline(object):
    def process_item(self, item, spider):
        if not AutoruAdv.objects.filter(autoru_id=item.get('autoru_id')).exists():
            item.save()
        return item