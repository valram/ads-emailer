import scrapy

from ads_checker.items import CarItem
from auto_ru.models import AutoruRubric


def extract_integer(text):
    return int(''.join([char for char in text if char.isdigit()]))


class AutoruSpider(scrapy.Spider):
    name = 'autoru'

    def __init__(self, url_prefix='/cars/used/', rubric=None, *args, **kwargs):
        if not rubric:
            self.rubric = AutoruRubric.objects.get(url_prefix=url_prefix)
        else:
            self.rubric = rubric
        self.start_urls = ['https://auto.ru{prefix}'.format(prefix=self.rubric.url_prefix), ]
        super(AutoruSpider, self).__init__(*args, **kwargs)

    _car_tag_selector = 'table > tbody.listing-item'
    _car_page_url_selector = 'a.link::attr(href)'
    _car_photo_url_selector = 'div.tile > div.tile__image-wrapper > img::attr(data-original)'
    _car_title_selector = 'tr:nth-child(1) > td.listing__cell.listing__cell_type_summary > div.listing-item__name > a::text'
    _car_year_selector = 'div.listing-item__year::text'
    _car_essentials_selector = 'tr:nth-child(1) > td.listing__cell.listing__cell_type_summary > div.listing-item__description::text'
    _car_price_selector = 'div.listing-item__price::text'
    _car_mileage_selector = 'div.listing-item__km::text'
    _car_location_selector = 'span.listing-item__place::text'

    def parse(self, response):
        for car_tag in response.css(self._car_tag_selector):

            title = car_tag.css(self._car_title_selector).extract_first()
            if not title:
                continue

            yield CarItem(
                title=title.strip(),
                year=extract_integer(car_tag.css(self._car_year_selector).extract_first()),
                mileage=extract_integer(car_tag.css(self._car_mileage_selector).extract_first()),
                price=extract_integer(car_tag.css(self._car_price_selector).extract_first()),
                location=car_tag.css(self._car_location_selector).extract_first(),
                essentials=car_tag.css(self._car_essentials_selector).extract_first().replace('\xa0', ' '),
                page_url=car_tag.css(self._car_page_url_selector).extract_first(),
                photo_url='https:{}'.format(car_tag.css(self._car_photo_url_selector).extract_first()),
                rubric=self.rubric,
                autoru_id=car_tag.css(self._car_page_url_selector).extract_first().split('/')[-2],
            )
