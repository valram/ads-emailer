from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from celery.utils.log import get_task_logger


from ads_emailer.celery import app
from ads_checker.spiders.autoru import AutoruSpider
from auto_ru.models import AutoruRubric


logger = get_task_logger(__name__)


@app.task
def crawl_auto_ru(*args, **kwargs):
    process = CrawlerProcess(get_project_settings())

    for rubric in AutoruRubric.objects.all():
        if rubric.subscribers.all().exists():
            process.crawl(AutoruSpider, rubric=rubric, *args, **kwargs)

    process.start()
