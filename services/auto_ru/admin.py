from django.contrib import admin


from .models import AutoruAdv, AutoruRubric


@admin.register(AutoruAdv)
class AutoruAdvAdmin(admin.ModelAdmin):

    list_display = [
        'title',
        'year',
        'mileage',
        'price',
        'location',
        'page_url',
    ]

@admin.register(AutoruRubric)
class AutoruRubricAdmin(admin.ModelAdmin):

    list_display = [
        'title',
        'url_prefix',
    ]
