from django.db import models

from subscribers.models import Subscriber


class AutoruAdv(models.Model):
    title = models.CharField(max_length=100,)
    year = models.PositiveIntegerField(null=True)
    mileage = models.PositiveIntegerField(null=True)
    price = models.PositiveIntegerField(null=True)
    location = models.CharField(max_length=50, null=True)
    essentials = models.TextField(max_length=1000, null=True)
    page_url = models.URLField()
    photo_url = models.URLField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    emailed = models.BooleanField(default=False)
    telegramed = models.BooleanField(default=False)
    autoru_id = models.CharField(max_length=50, null=True)
    rubric = models.ForeignKey('auto_ru.AutoruRubric',
                               on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.title


class AutoruRubric(models.Model):
    title = models.CharField(max_length=50, unique=True)
    url_prefix = models.CharField(max_length=250, unique=True)
    subscribers = models.ManyToManyField(
        Subscriber, related_name='autoru_subscriptions', blank=True)

    def __str__(self):
        return self.title
