import os
import sys


import telebot


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.append(os.path.abspath(BASE_DIR))

os.environ['DJANGO_SETTINGS_MODULE'] = 'ads_emailer.settings'

import django
django.setup()


from django.conf import settings
from subscribers.models import Subscriber
from auto_ru.models import AutoruRubric, AutoruAdv

TOKEN = settings.TELEGRAM_BOT_TOKEN


def listener(messages):
    for m in messages:
        if m.content_type == 'text':
            print('{name} [{chat_id}]: {text}'.format(name=m.chat.first_name,
                                                      chat_id=m.chat.id, text=m.text))


bot = telebot.TeleBot(TOKEN)
bot.set_update_listener(listener)


rubrics = set([r.title for r in AutoruRubric.objects.all()])


if settings.DEBUG:
    @bot.message_handler(commands=['reset',])
    def reset_db(message):
        cid = message.chat.id
        AutoruAdv.objects.update(telegramed=False, emailed=False)
        bot.send_message(cid, 'done')


@bot.message_handler(commands=['start', 'help'])
def start(message):
    cid = message.chat.id
    reply_message = """
    To subscribe you should 
    step 1: /email <email> -- provide your email, 
    step 2: verify email by clicking a link in activation email, 
    step 3: /rubrics -- shows all available rubrics to subscribe
    step 4: /subme <rubric_name> <rubric_name> -- choose subscribtions
    step 5: /del <rubric_name> -- delete your subscribtion (not implemented yet)
    step 6: /deactivate -- deactivate account (not implemented yet)
    """
    # TODO emplement step 5, 6
    bot.send_chat_action(cid, 'typing')
    bot.send_message(cid, reply_message)


@bot.message_handler(commands=['email'])
@bot.message_handler(regexp=r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$',
                     content_types=['text'])
def register_email(message):
    # TODO add email verification
    cid = message.chat.id
    command, email  = message.text.split()
    bot.send_chat_action(cid, 'typing')


    if Subscriber.objects.filter(email=email, telegram_id=cid).exists():
        bot.send_message(cid, 'You are already registered')

    elif Subscriber.objects.filter(email=email).exists():
        bot.send_message(cid, 'User with such email already exists')

    else:
        Subscriber.objects.create(
            email=email,
            activated=True,
            telegram_id=cid,
        )
        bot.send_message(cid, 'Your email has been registered, you can choose subscription')


@bot.message_handler(commands=['rubrics',])
def show_available_rubrics_to_subscribe(message):
    cid = message.chat.id
    bot.send_chat_action(cid, 'typing')
    available_rubrics_to_subscribe = '\n'.join(rubrics)
    bot.send_message(cid, available_rubrics_to_subscribe)


@bot.message_handler(commands=['subme',])
def subscribe_user(message):
    cid = message.chat.id
    command, *chosen_rubrics = message.text.split()
    chosen_rubrics_set = set(chosen_rubrics)
    bot.send_chat_action(cid, 'typing')

    if not chosen_rubrics_set.issubset(rubrics):
        bot.send_message(cid, 'You shoold choose rubrics from the following list')
        available_rubrics_to_subscribe = '\n'.join(rubrics)
        bot.send_message(cid, available_rubrics_to_subscribe)

    elif not Subscriber.objects.filter(telegram_id=cid).exists():
        bot.send_message(cid, 'You shoold register email before subscribtion')

    else:
        new_subscriber = Subscriber.objects.get(telegram_id=cid)
        for rubric in chosen_rubrics_set:
            rubric = AutoruRubric.objects.get(title=rubric)
            rubric.subscribers.add(new_subscriber)

        bot.send_message(cid, 'You have been subscribed on {}'.format(
            ', '.join(chosen_rubrics_set)))


if __name__ == '__main__':
    bot.polling(none_stop=True)
