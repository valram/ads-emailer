import os


from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ads_emailer.settings')

app = Celery('ads_emailer',)

app.config_from_object('django.conf:settings')

app.autodiscover_tasks()
