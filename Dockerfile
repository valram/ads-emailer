FROM python:3.6

ENV INSTALL_PATH /ads_emailer

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

ADD ./requirements.txt $INSTALL_PATH/requirements.txt

RUN pip install -r requirements.txt

ADD ./services/ $INSTALL_PATH
