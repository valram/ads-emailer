### Features

- auto.ru crawler by rubrics
- telegram bot to get email and telegram subscriptions
- dynamic from admin periodic tasks setup
- dynamic from admin rubrics and url-prefixes setup

### How to start with docker compose

```
$ cd project
$ sudo docker-compose up --build # to start
$ sudo docker-compose down # to stop

```
- For demo porposes db has been included into the project

- Visit auto_father_bot at telegram, and set up subscriber and subscribtions. 
Start with typing /help

- In debug mode there is available /reset command to set telegrammed and emailed fields to False

- Visit http://localhost:5000/admin/ (login: rvs, password: 123qwe123) to activate crawling
and emailing tasks 

- Emails can be seen at CLI

